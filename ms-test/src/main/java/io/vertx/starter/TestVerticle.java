package io.vertx.starter;

import java.util.ArrayList;

import io.vertx.blueprint.microservice.common.RestAPIVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class TestVerticle extends RestAPIVerticle {

  private static final String SERVICE_NAME = "api-rest";
  private static final String API_NAME = "api-test";
  private static final Logger logger = LoggerFactory.getLogger(TestVerticle.class);

  private static final String API_ADD = "/user";
  private static final String API_RETRIEVE = "/user/:id";
  private static final String API_RETRIEVE_ALL = "/user";
  private static final String API_UPDATE = "/user/:id";
  private static final String API_DELETE = "/user/:id";

  @Override
  public void start(Promise<Void> future) throws Exception {
    super.start();
    final Router router = Router.router(vertx);
    // body handler
    router.route().handler(BodyHandler.create());
    // api route handler
    router.post(API_ADD).handler(this::apiAddUser);
    router.get(API_RETRIEVE).handler(this::apiRetrieveUser);
    router.get(API_RETRIEVE_ALL).handler(this::apiRetrieveAll);
    router.patch(API_UPDATE).handler(this::apiUpdateUser);
    router.delete(API_DELETE).handler(this::apiDeleteUser);

    String host = config().getString("user.account.http.address", "localhost");
    int port = config().getInteger("user.account.http.port", 8081);
    // create HTTP server and publish REST service
    // createHttpServer(router, host, port)
    // .compose(serverCreated -> publishHttpEndpoint(SERVICE_NAME, host, port,
    // API_NAME)).setHandler(future);

    vertx.createHttpServer(new HttpServerOptions()).requestHandler(router).listen(port, host, ar -> { // (14)
      if (ar.succeeded()) {
        publishHttpEndpoint(SERVICE_NAME, host, port, API_NAME);
        future.complete();
        logger.info("<" + SERVICE_NAME + ">/<" + API_NAME + "> is running on port " + port);
      } else {
        future.fail(ar.cause());
      }
    });
  }

  private void apiAddUser(RoutingContext context) {
    context.getBodyAsJson();
    resultVoidHandler(context, 201);
  }

  private void apiRetrieveUser(RoutingContext context) {
    String id = context.request().getParam("id");
    resultHandlerNonEmpty(context);
  }

  private void apiRetrieveAll(RoutingContext context) {
    Future.succeededFuture(new ArrayList<String>()).setHandler(resultHandler(context, Json::encodePrettily));
  }

  private void apiUpdateUser(RoutingContext context) {
    notImplemented(context);
  }

  private void apiDeleteUser(RoutingContext context) {
    String id = context.request().getParam("id");
  }

}
