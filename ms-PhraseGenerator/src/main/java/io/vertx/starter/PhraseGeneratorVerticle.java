package io.vertx.starter;

import io.vertx.blueprint.microservice.common.RestAPIVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class PhraseGeneratorVerticle extends RestAPIVerticle {

  private static final Logger logger = LoggerFactory.getLogger(PhraseGeneratorVerticle.class);

  private static final String SERVICE_NAME = "phrase-generator";
  private static final String API_PREFIX = "phrase";

  private static final String API_ADD = "/generate";

  @Override
  public void start(Promise<Void> future) throws Exception {
    super.start();
    final Router router = Router.router(vertx);
    // body handler
    router.route().handler(BodyHandler.create());
    // api route handler
    router.get(API_ADD).handler(this::apiGeneratePhrase);

    String host = config().getString("user.account.http.address", "localhost");
    int port = config().getInteger("user.account.http.port", 8081);

    vertx.createHttpServer(new HttpServerOptions()).requestHandler(router).listen(port, host, ar -> { // (14)
      if (ar.succeeded()) {
        publishHttpEndpoint(SERVICE_NAME, host, port, API_PREFIX);
        future.complete();
        logger.info("<" + SERVICE_NAME + ">/<" + API_PREFIX + "> is running on port " + port);
      } else {
        future.fail(ar.cause());
      }
    });
  }

  private void apiGeneratePhrase(RoutingContext context) {
    // todo llamar a ms-getphrase
    String phrase = "How to express this ``you are walking on a road and there is a hole in front of you, you accidentally drop 1 of your legs into it``?";

    // todo traducirla
    phrase = "Como expresar esto `` estas caminando por una carretera y hay un agujero frente a ti, accidentalmente caes 1 de tus piernas en el ''?";
    // todo generate author
    String author = "Big Jimmy";

    // todo llamar a ms-getimage
    String imageUrl = "https://usercontent2.hubstatic.com/14289041_f1024.jpg";

    // todo guardar en base de datos y propagar id

    // todo con id llamar a ms-PhraseViewer
    String phraseHtml = generatePhraseHtml(phrase, author, imageUrl);

    // todo devolver la respuesta de ms-PhraseViewer

    Future.succeededFuture(phraseHtml).setHandler(rawResultHandler(context));
  }

  private String generatePhraseHtml(String phrase, String author, String imageUrl) {

    StringBuilder myvar = new StringBuilder();
    myvar.append(
        "<style type=\"text/css\">    .tg  {border-collapse:collapse;border-spacing:0;}    .tg td{padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}    .tg th{font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}    .tg .tg-0lax{vertical-align:top}      table{        width:666px;      }      .cursive{        font-family:cursive;  text-align:left;       font-size: 18px;      }      .author{        font-size: 12px; font-family:sans-serif     }      .link{     font-size:12px;   font-weight:bold;        text-align: right;      }    </style>    <table class=\"tg\">      <tr>        <th class=\"tg-0lax cursive\" colspan=\"2\">@phrase@</th>        <th class=\"tg-0lax\" rowspan=\"2\"><img height=\"120\" src=\"@imageUrl@\"/></th>      </tr>      <tr>        <td class=\"tg-0lax author\">@author@</td>        <td class=\"tg-0lax link\"><a href=\"@phraseLikeUrl@\">ME GUSTA</a></td>      </tr>    </table>");
    String phraseHtml = myvar.toString();
    phraseHtml = phraseHtml.replaceAll("@phrase@", phrase);
    phraseHtml = phraseHtml.replaceAll("@author@", author);
    phraseHtml = phraseHtml.replaceAll("@imageUrl@", imageUrl);
    phraseHtml = phraseHtml.replaceAll("@phraseLikeUrl@", "http://localhost:3333/api/phrase/1/like");
    return phraseHtml;
  }

}
