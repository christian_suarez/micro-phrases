package io.vertx.starter;

import java.util.List;
import java.util.Optional;

import io.vertx.blueprint.microservice.common.RestAPIVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.HttpEndpoint;

public class MainVerticle extends RestAPIVerticle {

  private static final int DEFAULT_PORT = 8787;

  private static final Logger logger = LoggerFactory.getLogger(MainVerticle.class);

  @Override
  public void start(Promise<Void> future) throws Exception {
    super.start();

    // get HTTP host and port from configuration, or use default value
    String host = config().getString("api.gateway.http.address", "localhost");
    int port = config().getInteger("api.gateway.http.port", DEFAULT_PORT); // (1)

    Router router = Router.router(vertx); // (2)

    // body handler
    router.route().handler(BodyHandler.create()); // (4)

    // version handler
    router.get("/api/v").handler(this::apiVersion); // (5)

    String hostURI = String.format("https://localhost:%d", port);

    // api dispatcher
    router.route("/api/*").handler(this::dispatchRequests); // (10)

    // static content
    router.route("/*").handler(StaticHandler.create()); // (12)

    // enable HTTPS
    HttpServerOptions httpServerOptions = new HttpServerOptions();

    // create http server
    vertx.createHttpServer(httpServerOptions).requestHandler(router).listen(port, host, ar -> { // (14)
      if (ar.succeeded()) {
        publishApiGateway(host, port);
        future.complete();
        logger.info("API Gateway is running on port " + port);
        // publish log
        publishGatewayLog("api_gateway_init_success:" + port);
      } else {
        future.fail(ar.cause());
      }
    });

  }

  private void apiVersion(RoutingContext context) {
    context.response().end(new JsonObject().put("version", "v1").encodePrettily());
  }

  private void dispatchRequests(RoutingContext context) {
    int initialOffset = 5; // length of `/api/`
    // run with circuit breaker in order to deal with failure
    circuitBreaker.execute(future -> {
      getAllEndpoints().future().setHandler(ar -> {
        if (ar.succeeded()) {
          List<Record> recordList = ar.result();
          // get relative path and retrieve prefix to dispatch client
          String path = context.request().uri();

          if (path.length() <= initialOffset) {
            notFound(context);
            future.complete();
            return;
          }
          String prefix = (path.substring(initialOffset).split("/"))[0];
          // generate new relative path
          String newPath = path.substring(initialOffset + prefix.length());
          // get one relevant HTTP client, may not exist
          Optional<Record> client = recordList.stream().map(record -> {
            logger.info(record.getName());
            return record;
          }).filter(record -> record.getMetadata().getString("api.name") != null)
              .filter(record -> record.getMetadata().getString("api.name").equals(prefix)).findAny(); // simple load
                                                                                                      // balance

          if (client.isPresent()) {
            logger.info(newPath);
            doDispatch(context, newPath, discovery.getReference(client.get()).getAs(WebClient.class), future);
          } else {
            notFound(context);
            future.complete();
          }
        } else {
          future.fail(ar.cause());
        }
      });
    }).setHandler(ar -> {
      if (ar.failed()) {
        badGateway(ar.cause(), context);
      }
    });
  }

  /**
   * Dispatch the request to the downstream REST layers.
   *
   * @param context routing context instance
   * @param path    relative path
   * @param client  relevant HTTP client
   */
  private void doDispatch(RoutingContext context, String path, WebClient client, Promise<Object> cbFuture) {
    HttpRequest<Buffer> req = client.raw(context.request().method().toString(), path);

    context.request().headers().forEach(header -> {
      req.putHeader(header.getKey(), header.getValue());
    });
    if (context.user() != null) {
      req.putHeader("user-principal", context.user().principal().encode());
    }

    // send request
    if (context.getBody() == null) {
      req.send(ar -> this.handleResponse(ar, context, client, cbFuture));
    } else {
      req.sendBuffer(context.getBody(), ar -> this.handleResponse(ar, context, client, cbFuture));
    }
  }

  private void handleResponse(AsyncResult<HttpResponse<Buffer>> ar, RoutingContext context, WebClient client,
      Promise<Object> cbFuture) {
    if (ar.succeeded()) {
      HttpResponse<Buffer> response = ar.result();
      Buffer body = response.body();
      if (response.statusCode() >= 500) { // api endpoint server error, circuit breaker should fail
        cbFuture.fail(response.statusCode() + ": " + body.toString());
      } else {
        HttpServerResponse toRsp = context.response().setStatusCode(response.statusCode());
        response.headers().forEach(header -> {
          toRsp.putHeader(header.getKey(), header.getValue());
        });
        // send response
        toRsp.end(body);
        cbFuture.complete();
      }
      ServiceDiscovery.releaseServiceObject(discovery, client);
    } else {
      cbFuture.fail(ar.cause());
    }
  }

  /**
   * Get all REST endpoints from the service discovery infrastructure.
   *
   * @return async result
   */
  private Promise<List<Record>> getAllEndpoints() {
    Promise<List<Record>> future = Promise.promise();
    discovery.getRecords(record -> {
      logger.info(record.getType());
      return record.getType().equals(HttpEndpoint.TYPE);
    }, future);
    return future;
  }

  // log methods

  private void publishGatewayLog(String info) {
    JsonObject message = new JsonObject().put("info", info).put("time", System.currentTimeMillis());
    publishLogEvent("gateway", message);
  }

}
